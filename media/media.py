class Media(object):
	def __init__(self, title='', path='', media_type='', genre='', description='', artpath=''):
		self.title = title
		self.path = path
		self.media_type = media_type
		self.genre = genre
		self.description = description
		self.artpath = artpath
	
	@property
	def Title(self):
		return self.title
	
	@Title.setter
	def Title(self, value):
		self.title = value
	
	@property
	def Path(self):
		return self.path
	
	@Path.setter
	def Path(self, value):
		self.path = value
	
	@property
	def Media_type(self):
		return self.media_type
	
	@Media_type.setter
	def Media_type(self, value):
		self.media_type = value
	
	@property
	def Genre(self):
		return self.genre
	
	@Genre.setter
	def Genre(self, value):
		self.genre = value
	
	@property
	def Description(self):
		return self.description
	
	@Description.setter
	def Description(self, value):
		self.description = value
	
	@property
	def Artpath(self):
		return self.artpath
	
	@Artpath.setter
	def Artpath(self, value):
		self.artpath = value
