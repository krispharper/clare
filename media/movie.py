import media

class Movie(media.Media):
	def __init__(self, title='', path='', type='', genre='', description='', artpath='', year='', actors='', director='', rating=''):
		super(Movie, self).__init__(title, path, 'Movie', genre, description, artpath)
		self.year = year
		self.actors = actors
		self.director = director
		self.rating = rating
	
	@property
	def Year(self):
		return self.year
	
	@Year.setter
	def Year(self, value):
		self.year = value
	
	@property
	def Actors(self):
		return self.actors
	
	@Actors.setter
	def Actors(self, value):
		self.actors = value
	
	@property
	def Director(self):
		return self.director
	
	@Director.setter
	def Director(self, value):
		self.director = value
	
	@property
	def Rating(self):
		return self.rating
	
	@Rating.setter
	def Rating(self, value):
		self.rating = value
	
