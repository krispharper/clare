import gtk
import constants
import prefs_manager

class ColumnChooser:
	_chkbtns = []

	def add_checkboxes(self):
		"""Add all checkboxes to the window."""
		column_labels = self.col_dict.keys()
		column_labels.sort()
		choices = self.prefs.get_cols_choices(self.media)
		for index, label in enumerate(column_labels):
			chkbtn = gtk.CheckButton(label)
			chkbtn.set_active(choices[self.col_dict[label]] == 'True')
			self._chkbtns.append(chkbtn)
			aligner = gtk.Alignment(0, 0, 0, 0)
			aligner.set_padding(0, 0, 5, 5)
			aligner.add(chkbtn)
			if index < len(column_labels)/2:
				self.vboxl.pack_start(aligner)
			if index >= len(column_labels)/2:
				self.vboxr.pack_start(aligner)

	def get_toggle_dict(self):
		"""Get which columns are toggleed. Retuns a dict."""
		toggle_dict = {}
		for chkbtn in self._chkbtns:
			toggle_dict[self.col_dict[chkbtn.get_label()]] = chkbtn.get_active()
		
		return toggle_dict
	
	def set_col_dict(self):
		"""Returns a dictionary of label : name pairs for columns."""
		if self.media == 'movies':
			column_params = constants.movie_list_columns
		for param in column_params:
			self.col_dict[param['label']] = param['name']

	def save_choices(self):
		"""Writes column choices to file and sets the view."""
		self.prefs.save_cols_choices(self.media, self.get_toggle_dict())
		self.view.current_view.set_column_visibility()
		self.window.hide()
	
	def on_save_btn_click(self, widget):
		self.save_choices()

	def __init__(self, view):
		self.media = view.current_view.media
		self.view = view
		self.col_dict = {}
		self.set_col_dict()
		self.prefs = prefs_manager.PrefsManager()

		self.window = gtk.Window()
		self.window.connect('destroy', lambda w: self.window.hide())
		self.window.set_resizable(False)
		self.window.set_title('Columns')
		self.window.set_position(gtk.WIN_POS_CENTER)

		vbox = gtk.VBox()
		vbox.set_spacing(20)
		hbox_top = gtk.HBox()
		hbox_top.set_spacing(10)
		hbox_bottom = gtk.HBox()
		hbox_bottom.set_spacing(10)
		self.vboxl = gtk.VBox()
		self.vboxr = gtk.VBox()

		save_btn = gtk.Button('Save')
		cncl_btn = gtk.Button('Cancel')
		save_btn.connect('clicked', self.on_save_btn_click)
		cncl_btn.connect('clicked', lambda w: self.window.hide())
		hbox_bottom.pack_start(save_btn)
		hbox_bottom.pack_start(cncl_btn)

		halign = gtk.Alignment(1,0,0,0)
		halign.set_padding(0, 5, 10, 10)
		halign.add(hbox_bottom)
		vbox.pack_end(halign)

		self.add_checkboxes()
		hbox_top.pack_start(self.vboxl)
		hbox_top.pack_start(self.vboxr)
		vbox.pack_start(hbox_top)

		self.window.add(vbox)
		self.window.show_all()
