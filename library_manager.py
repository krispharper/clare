import os
import gtk
import tag_manager
import constants
import prefs_manager
import media.movie
from lxml import etree

class MovieLibraryManager:
	@staticmethod
	def open_library():
		"""Gets the library root from file and returns an etree."""
		if not os.path.exists(constants.main_dir):
			os.mkdir(constants.main_dir)
		
		if not os.path.exists(constants.movie_library):
			return etree.ElementTree(etree.Element('library'))
		else:
			parser = etree.XMLParser(remove_blank_text=True)
			return etree.parse(constants.movie_library, parser)
	
	def load_library(self):
		return []

	@staticmethod
	def exists_in_library(movie):
		root = MovieLibraryManager.open_library()
		return bool(root.xpath('//item[path="' + movie.Path + '"]'))
	
	@staticmethod
	def delete_from_library(movie):
		if MovieLibraryManager.exists_in_library(movie):
			root = MovieLibraryManager.open_library()
			items = root.xpath('//item[path="' + movie.Path + '"]')
			root.getroot().remove(items[0])
			root.write(constants.movie_library, pretty_print=True)

	@staticmethod
	def add_to_library(movie):
		MovieLibraryManager.delete_from_library(movie)
		item = tag_manager.TagManager.make_tree(movie.Path)
		properties = movie.__dict__

		for prop in properties:
			if properties[prop]:
				etree.SubElement(item, prop).text = unicode(properties[prop])


		root = MovieListManager.open_library()
		root.getroot().append(item)
		root.write(constants.movie_library, pretty_print=True)
	
	@staticmethod
	def get_item_from_library(path):
		movie = media.movie.Movie()
		root = MovieLibraryManager.open_library()
		matching_items = root.xpath('//item[path="' + path + '"]')
		if matching_items:
			movie_tree = matching_items[0]
			for prop in movie.__dict__:
				node = movie_tree.find(prop)
				if node is not None:
					movie.__dict__[prop] = node.text
		
		return movie
	
	@staticmethod
	def mark_as_watched(paths):
		root = MovieLibraryManager.open_library()

		for path in paths:
			matching_items = root.xpath('//item[path="' + path + '"]')
			if matching_items:
				movie_tree = matching_items[0]
				for child in movie_tree:
					if child.tag == 'watched':
						child.text = 'True'
		
		etree.ElementTree(root).write(constants.movie_library, pretty_print=True)

	def __init__(self):
		self.prefs = prefs_manager.PrefsManager()

class MovieListManager(MovieLibraryManager):
	def load_library(self):
		library = self.open_library()
		movies = library.getroot().findall('item')
		movie_list = []

		for movie in movies:
			row = []
			for param in constants.movie_list_columns:
				if movie.find(param['name']) is not None:
					if param['type'] == bool:
						row.append(movie.find(param['name']).text == 'True')
					else:
						row.append(movie.find(param['name']).text)
				else:
					row.append(None)
			movie_list.append(row)

		return movie_list

class MovieGridManager(MovieLibraryManager):
	def load_library(self):
		library = self.open_library()
		movies = library.getroot().findall('item')
		movie_list = []

		for movie in movies:
			row = []
			artpath = movie.find('artpath')
			if artpath is not None:
				dir_pair = os.path.split(artpath.text)
				parent = os.path.join(os.path.dirname(dir_pair[0]), '200')
				scaled_path = os.path.join(parent, dir_pair[1])
				pixbuf = gtk.gdk.pixbuf_new_from_file(scaled_path)
				#pixbuf = pixbuf.scale_simple(264, 400, gtk.gdk.INTERP_BILINEAR)
				row.append(pixbuf)
			else:
				row.append(None)

			title = movie.find('title')
			if title is not None:
				title_text = title.text
				if len(title_text) > 23:
					short_text = title_text[:10] + '...' + title_text[-10:]
					row.append(short_text)
				else:
					row.append(title_text)
			else:
				row.append(None)

			for param in constants.movie_grid_columns:
				if movie.find(param['name']) is not None:
					row.append(movie.find(param['name']).text)
				else:
					row.append(None)

			movie_list.append(row)

		return movie_list
