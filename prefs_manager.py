from lxml import etree
import constants
import os.path

class PrefsManager:
	def check_directories(self):
		directories = [
			constants.main_dir,
			constants.artwork_dir,
			constants.movie_artwork_dir,
			constants.movie_art_originals]

		for size in constants.artwork_sizes:
			directories.append(os.path.join(constants.movie_artwork_dir, size))

		for directory in directories:
			if not os.path.exists(directory):
				os.mkdir(directory)

	def get_prefs_root(self):
		parser = etree.XMLParser(remove_blank_text=True)
		return etree.parse(constants.prefs_file, parser).getroot()
		
	def create_new_dirs_tree(self, movies_path, tv_shows_path):
		"""Writes default directories to a preferences file."""
		root = etree.Element('prefs')
		movies = etree.SubElement(root, 'movies')
		etree.SubElement(movies, 'dir').text = movies_path
		tv_shows = etree.SubElement(root, 'tv-shows')
		etree.SubElement(tv_shows, 'dir').text = tv_shows_path
		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)

	def create_new_cols_tree(self):
		"""Writes defalt column preferences to a file."""
		root = self.get_prefs_root()
		movies = root.find('movies')
		movie_cols = etree.SubElement(movies, 'cols')

		for index, col_name in enumerate([param['name'] for param in constants.movie_list_columns]):
			movie_col = etree.SubElement(movie_cols, 'col')
			movie_col.attrib['name'] = col_name
			movie_col.attrib['show'] = 'True'
			movie_col.attrib['position'] = str(index)

			if col_name == 'title':
				movie_col.attrib['active'] = 'True'
			else:
				movie_col.attrib['active'] = 'False'
		
		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)
	
	def set_default_view(self):
		"""Sets the default view preferences to movie and list."""
		root = self.get_prefs_root()
		view = etree.SubElement(root, 'view').attrib
		view['media'] = 'movies'
		view['layout'] = 'list'
		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)
	
	def set_default_size(self):
		"""Sets the default width and height of the main window."""
		root = self.get_prefs_root()
		size = etree.SubElement(root, 'size').attrib
		size['width'] = '1000'
		size['height'] = '600'
		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)
	
	def get_size_prefs(self):
		"""Gets the preferences for main window size from file. Returns a dict."""
		root = self.get_prefs_root()
		size = root.find('size').attrib
		return (int(size['width']), int(size['height']))
	
	def set_size_prefs(self, width, height):
		"""Save the size settings to a file."""
		root = self.get_prefs_root()
		size = root.find('size').attrib
		size['width'] = width
		size['height'] = height
		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)

	def get_view_prefs(self):
		"""Gets the preferences for media type and layout from file. Returns a dict."""
		root = self.get_prefs_root()
		view = root.find('view').attrib
		return {'media' : view['media'], 'layout' : view['layout']}
	
	def set_view_prefs(self, media, layout):
		"""Save the view settings to a file."""
		root = self.get_prefs_root()
		view = root.find('view').attrib
		view['media'] = media
		view['layout'] = layout
		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)

	def save_dirs_prefs(self, movies_path, tv_shows_path):
		"""Saves the library directories to a preferences file."""
		root = self.get_prefs_root()
		movies = root.find('movies')
		movies.find('dir').text = movies_path
		tv_shows = root.find('tv-shows')
		tv_shows.find('dir').text = tv_shows_path
		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)

	def get_dirs_prefs(self):
		"""Gets the directories for each library. Returns a dict."""
		root = self.get_prefs_root()
		return {'movies path' : root.find('movies').find('dir').text,
				'tv shows path' : root.find('tv-shows').find('dir').text}

	def save_cols_choices(self, media, col_dict):
		"""Save the visibility preferences for each column."""
		root = self.get_prefs_root()
		cols = root.find(media).find('cols')
		for col in cols:
			col.attrib['show'] = str(col_dict[col.attrib['name']])

		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)

	def get_cols_choices(self, media):
		"""Get the visibility preferences for each column. Returns a dict."""
		root = self.get_prefs_root()
		cols = root.find(media).find('cols')
		tag_dict = {}
		for col in cols:
			tag_dict[col.attrib['name']] = col.attrib['show']

		return tag_dict

	def save_cols_positions(self, media, col_dict):
		"""Saves the position preferences for each column."""
		root = self.get_prefs_root()
		cols = root.find(media).find('cols')
		for col in cols:
			col.attrib['position'] = str(col_dict[col.attrib['name']])

		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)

	def get_cols_positions(self, media):
		"""Get the positions preferences for each column. Returns a dict."""
		root = self.get_prefs_root()
		cols = root.find(media).find('cols')
		tag_dict = {}
		for col in cols:
			tag_dict[col.attrib['name']] = col.attrib['position']

		return tag_dict
	
	def save_cols_widths(self, media, col_dict):
		"""Saves the width of each column to the prefs file."""
		root = self.get_prefs_root()
		cols = root.find(media).find('cols')
		for col in cols:
			col.attrib['width'] = str(col_dict[col.attrib['name']])

		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)

	def get_cols_widths(self, media):
		"""Get the positions preferences for each column. Returns a dict."""
		root = self.get_prefs_root()
		cols = root.find(media).find('cols')
		tag_dict = {}
		for col in cols:
			tag_dict[col.attrib['name']] = col.attrib['width']

		return tag_dict
	
	def save_active_col(self, media, column_name):
		"""Save the currently sorted column."""
		root = self.get_prefs_root()
		cols = root.find(media).find('cols')
		for col in cols:
			if col.attrib['name'] == column_name:
				col.attrib['active'] = 'True'
			else:
				col.attrib['active'] = 'False'

		etree.ElementTree(root).write(constants.prefs_file, pretty_print=True)
	
	def get_active_col(self, media):
		"""Get the saved column by which to sort."""
		root = self.get_prefs_root()
		cols = root.find(media).find('cols')
		for col in cols:
			if col.attrib['active'] == 'True':
				return col.attrib['position']

		return '0'
	
	def __init__(self):
		self.check_directories()
		if not os.path.exists(constants.prefs_file):
			self.create_new_dirs_tree(constants.default_movies_path, constants.default_tv_shows_path)
			self.create_new_cols_tree()
			self.set_default_view()
			self.set_default_size()
