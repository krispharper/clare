import gtk
import prefs_manager

class PrefsWindow:
	def on_prefs_window_destroy(self, widget, data=None):
		self.prefs_window.hide()
	
	def on_save_click(self, widget, data=None):
		movies_path = self.movie_chooser.get_filename()
		tv_shows_path = self.tv_show_chooser.get_filename()
		self.prefs.save_dirs_prefs(movies_path, tv_shows_path)
		self.prefs_window.hide()

	def __init__(self):
		self.prefs = prefs_manager.PrefsManager()

		builder = gtk.Builder()
		builder.add_from_file('prefs_window.glade') 

		self.prefs_window = builder.get_object('prefs_window')
		self.movie_chooser = builder.get_object('movie_chsr')
		self.tv_show_chooser = builder.get_object('tv_show_chsr')

		dir_dict = self.prefs.get_dirs_prefs()
		self.movie_chooser.set_filename(dir_dict['movies path'])
		self.tv_show_chooser.set_filename(dir_dict['tv shows path'])

		builder.connect_signals(self)

if __name__ == "__main__":
	app = PrefsWindow()
	app.prefs_window.show()
	gtk.main()

