import gtk
import prefs_window
import list_manager
import column_chooser
import prefs_manager

class MainWindow:

	def on_main_window_destroy(self, widget, data=None):
		if self.view.current_view.layout == 'list':
			self.view.current_view.save_column_order()
			self.view.current_view.save_column_widths()
			self.view.current_view.save_sorted_column()

		self.prefs.set_view_prefs(self.view.current_view.media, self.view.current_view.layout)
		gtk.main_quit()
	
	def on_main_window_delete(self, widget, data=None):
		width = str(self.main_window.get_size()[0])
		height = str(self.main_window.get_size()[1])
		self.prefs.set_size_prefs(width, height)
		return False

	def prefs_menu_item_activate(self, widget, data=None):
		window = prefs_window.PrefsWindow()
		#window.prefs_window.set_transient_for(self.main_window)
		window.prefs_window.show()
	
	def column_chooser_menu_item_activate(self, widget, data=None):
		column_chooser.ColumnChooser(self.view)
	
	def on_movie_tgl_clicked(self, widget, data=None):
		toggle_val = self.movie_toggle.get_active()
		self.tv_toggle.set_active(not toggle_val)

	def on_tv_tgl_clicked(self, widget, data=None):
		toggle_val = self.tv_toggle.get_active()
		self.movie_toggle.set_active(not toggle_val)
	
	def on_list_tgl_clicked(self, widget, data=None):
		toggle_val = self.list_toggle.get_active()
		self.grid_toggle.set_active(not toggle_val)

		media = self.view.current_view.media
		if toggle_val:
			self.view.set_current_view(media, 'list', self.scrolled_window)
	
	def on_grid_tgl_clicked(self, widget, data=None):
		toggle_val = self.grid_toggle.get_active()
		self.list_toggle.set_active(not toggle_val)

		media = self.view.current_view.media
		if toggle_val:
			self.view.set_current_view(media, 'grid', self.scrolled_window)

	def set_view(self):
		view_dict = self.prefs.get_view_prefs()
		
		if view_dict['media'] == 'movies':
			self.movie_toggle.set_active(True)
		elif view_dict['media'] == 'tv-shows':
			self.tv_toggle.set_active(True)
		
		if view_dict['layout'] == 'list':
			self.list_toggle.set_active(True)
		elif view_dict['layout'] == 'grid':
			self.grid_toggle.set_active(True)

		self.view.set_current_view(view_dict['media'], view_dict['layout'], self.scrolled_window)

	def __init__(self):
		self.prefs = prefs_manager.PrefsManager()

		builder = gtk.Builder()
		builder.add_from_file('main_window.glade') 

		self.main_window = builder.get_object('main_window')
		self.statusbar = builder.get_object('statusbar')
		self.scrolled_window = builder.get_object('scrolledwindow')
		self.movie_toggle = builder.get_object('movie_tgl')
		self.tv_toggle = builder.get_object('tv_tgl')
		self.list_toggle = builder.get_object('list_tgl')
		self.grid_toggle = builder.get_object('grid_tgl')

		self.main_window.set_default_size(*self.prefs.get_size_prefs())

		self.scrolled_window.hide()
		self.view = list_manager.MainView()
		self.set_view()
		self.scrolled_window.show_all()

		self.statusbar.push(0, "Total items: ")

		builder.connect_signals(self)

if __name__ == "__main__":
	app = MainWindow()
	app.main_window.show()
	gtk.main()
