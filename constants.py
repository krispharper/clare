import gtk
import os.path
import datetime

main_dir = os.path.expanduser('~/.config/clare')
artwork_dir = os.path.join(main_dir, 'artwork')
movie_artwork_dir = os.path.join(artwork_dir, 'movies')
movie_art_originals = os.path.join(movie_artwork_dir, 'originals')
movie_library = os.path.join(main_dir, 'movie_library.xml')
prefs_file = os.path.join(main_dir, 'prefs.xml')
default_movies_path = os.path.expanduser('~/Videos/Movies')
default_tv_shows_path = os.path.expanduser('~/Videos/TV Shows')

cell_rend_text = gtk.CellRendererText()
cell_rend_toggle = gtk.CellRendererToggle()

def time_func(column, cell, model, treeiter, index):
	value = model.get_value(treeiter, index) or '0'
	cell.set_property('text', str(datetime.timedelta(seconds = int(value))))

def size_func(column, cell, model, treeiter, index):
	value = model.get_value(treeiter, index) or '0'

	def format_size(size):
		if 0 < size < 1000:
			return ('%f' % float(size)).rstrip('0').rstrip('.') + ' B'
		elif 1000 <= size < 1000000:
			return '{0:.2f}'.format(float(size)/1000).rstrip('0').rstrip('.') + ' KB'
		elif 1000000 <= size < 1000000000:
			return '{0:.2f}'.format(float(size)/1000000).rstrip('0').rstrip('.') + ' MB'
		elif 1000000000 <= size < 1000000000000:
			return '{0:.2f}'.format(float(size)/1000000000).rstrip('0').rstrip('.') + ' GB'
		elif 1000000000000 <= size < 1000000000000000:
			return '{0:.2f}'.format(float(size)/1000000000000).rstrip('0').rstrip('.') + ' TB'
		else:
			return str(size)

	cell.set_property('text', format_size(int(value)))

movie_list_columns = (
						{'label' : "",
						'name' : 'watched',
						'type' : bool,
						'renderer' : cell_rend_toggle,
						'func' : None},
						{'label' : "Title",
						'name' : 'title',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Genre",
						'name' : 'genre',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Year",
						'name' : 'year',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Time",
						'name' : 'length',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : time_func},
						{'label' : "Size",
						'name' : 'size',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : size_func},
						{'label' : "Description",
						'name' : 'description',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Rating",
						'name' : 'rating',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Actors",
						'name' : 'actors',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Director",
						'name' : 'director',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Type",
						'name' : 'type',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Encoder",
						'name' : 'encoder',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Codec",
						'name' : 'codec',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Width",
						'name' : 'width',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Height",
						'name' : 'height',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Date Added",
						'name' : 'date-added',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
						{'label' : "Path",
						'name' : 'path',
						'type' : str,
						'renderer' : cell_rend_text,
						'func' : None},
					)

movie_grid_columns = (
						{'name' : 'title',
						'type' : str},
						{'name' : 'genre',
						'type' : str},
						{'name' : 'year',
						'type' : str},
						{'name' : 'length',
						'type' : str},
						{'name' : 'size',
						'type' : str},
						{'name' : 'description',
						'type' : str},
						{'name' : 'rating',
						'type' : str},
						{'name' : 'actors',
						'type' : str},
						{'name' : 'type',
						'type' : str},
						{'name' : 'encoder',
						'type' : str},
						{'name' : 'codec',
						'type' : str},
						{'name' : 'width',
						'type' : str},
						{'name' : 'height',
						'type' : str},
						{'name' : 'date-added',
						'type' : str},
						{'name' : 'path',
						'type' : str},
					)

kaa_tags = ('length',
			'codec',
			'width',
			'height')

ratings = ( 'G',
			'PG',
			'PG-13',
			'R',
			'NC-17',
			'Unrated')

genres = (	'Action',
			u'Children\u2019s',
			'Comedy',
			'Documentary',
			'Drama',
			'Horror',
			'Musical',
			'Science Fiction',
			'Western')

artwork_sizes = ('32',
				'64',
				'128',
				'192',
				'256',
				'384')
