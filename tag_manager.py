import os.path
import datetime
from constants import kaa_tags
from lxml import etree
from kaa import metadata

class TagManager:
	@staticmethod
	def get_tag_dict(path):
		"""Gets all the tags for a file and returns a dict."""
		if not os.path.exists(path):
			return {}

		tag_dict = TagManager.get_kaa_tags(path)
		tag_dict['size'] = os.path.getsize(path)
		tag_dict['date-added'] = datetime.date.today()
		return tag_dict

	@staticmethod
	def get_kaa_tags(path):
		"""Gets the kaa metadata tags for a file. Returns a dict."""
		movie_media = metadata.parse(path);
		tag_dict = {}
		
		if movie_media.has_key('type'):
			tag_dict['type'] = movie_media.get('type')
		else:
			tag_dict['type'] = None

		for tag in kaa_tags:
			if movie_media.video[0].has_key(tag):
				tag_dict[tag] = movie_media.video[0].get(tag)
		
		return tag_dict

	@staticmethod
	def make_tree(path):
		"""Makes an XML tree out of the metadata tags. Returns an Element."""
		tag_dict = TagManager.get_tag_dict(path)
		tree = etree.Element('item')
		for tag, value in tag_dict.iteritems():
			etree.SubElement(tree, tag).text = unicode(value)
		return tree

if __name__ == "__main__":
	man = TagManager()
