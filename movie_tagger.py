import constants
import media.movie as movie
import gtk
import os.path
import shutil
import library_manager

class MovieTagger:
	def on_window_destroy(self, widget, data=None):
		"""Destroy signal"""
		self.tag_window.hide()

		if self.artwork_set:
			os.remove(self.art_path)
		#gtk.main_quit()
	
	def on_save_btn_clicked(self, widget, data=None):
		"""Save button signal
		
		Tags movie, renames if needed and makes artwork thumbs.

		"""
		self.tag_movie()
		if self.artwork_set:
			self.add_artwork_thumbs()
		self.tag_window.hide()
	
	def on_art_view_focus_in(self, widget, data=None):
		"""Focus on artwork signal"""
		self.art_view.select_path(0)
	
	def on_art_view_focus_out(self, widget, data=None):
		"""Unfocus on artwork signal"""
		self.art_view.unselect_path(0)
	
	def on_art_view_button_press_event(self, widget, event):
		"""Artwork button press signal

		Checks for right click and pops up a menu

		"""
		if event.button == 3:
			if not self.clipboard.wait_for_image():
				self.paste_item.set_sensitive(False)
			self.popup.popup(None, None, None, event.button, event.time)
	
	def on_paste_item_activate(self, widget, data=None):
		"""Paste signal"""
		self.update_artwork_from_clipboard()
	
	def update_artwork(self, new_art, format='png', extension='.png'):
		"""Update the artwork

		Saves to a temp file and updates the IconView

		"""
		if new_art:
			self.art_path = os.path.splitext(self.art_path)[0] + extension
			new_art.save(self.art_path, format)
			self.artwork_set = True
			dims = self.get_new_dimensions(new_art, 133)
			new_art_scaled = new_art.scale_simple(dims[0], dims[1], gtk.gdk.INTERP_BILINEAR)
			self.art_store.clear()
			self.art_store.append((new_art_scaled,))
	
	def update_artwork_from_clipboard(self):
		"""Updates the artwork from the clipboard"""
		self.update_artwork(self.clipboard.wait_for_image())
	
	def update_artwork_from_file(self, path):
		"""Updates artwork from a file path"""
		format_dict = gtk.gdk.pixbuf_get_file_info(path)[0]
		extension = os.path.splitext(path)[1]
		artwork = gtk.gdk.pixbuf_new_from_file(path)

		if format_dict:
			self.update_artwork(artwork, format_dict['name'], extension)
		else:
			self.update_artwork(artwork)
	
	def get_new_dimensions(self, artwork, scaled_width):
		"""Gets new dimensions for a scaled image given a new width"""
		height = artwork.get_height()
		width = artwork.get_width()

		scaled_height = int(float(scaled_width) * 1.5)

		if float(width) / float(height) >= float(scaled_width) / float(scaled_height):
			return (scaled_width, int(scaled_width * float(height) / float(width)))
		else:
			return (int(scaled_height * float(width) / float(height)), scaled_height)
	
	def add_artwork_thumbs(self):
		"""Creates thumbnails for artwork

		Given the current temporary artwork, creates thumbials
		of sizes set in the constants file

		"""
		artwork = gtk.gdk.pixbuf_new_from_file(self.art_path)
		extension = os.path.splitext(self.art_path)[1]
		format_dict = gtk.gdk.pixbuf_get_file_info(self.art_path)[0]
		format = 'png'

		if format_dict:
			format = format_dict['name']
		
		artwork_name = os.path.splitext(os.path.split(self.movie.Path)[1])[0] + extension
		shutil.copyfile(self.art_path, os.path.join(constants.movie_art_originals, artwork_name))

		for size in constants.artwork_sizes:
			dims = self.get_new_dimensions(artwork, int(size))
			scaled_artwork = artwork.scale_simple(dims[0], dims[1], gtk.gdk.INTERP_HYPER)
			scaled_artwork_dir = os.path.join(constants.movie_artwork_dir, size)
			scaled_artwork.save(os.path.join(scaled_artwork_dir, artwork_name), format)

		os.remove(self.art_path)
	
	def tag_movie(self):
		"""Sets all the tags for the movie based on current values"""
		self.movie.Title = self.title_entry.get_text()
		self.movie.Genre = self.genre_cmb.child.get_text()
		self.movie.Year = self.year_entry.get_text()
		self.movie.Actors = self.actors_entry.get_text()
		self.movie.Rating = self.rating_cmb.get_active_text()
		self.movie.Director = self.director_entry.get_text()
		desc_buff = self.description_entry.get_buffer()
		self.movie.Description = desc_buff.get_text(desc_buff.get_start_iter(), desc_buff.get_end_iter())
		if self.artwork_set:
			extension = os.path.splitext(self.art_path)[1]
			artwork_name = os.path.splitext(os.path.split(self.movie.Path)[1])[0] + extension
			self.movie.Artpath = os.path.join(constants.movie_art_originals, artwork_name)

		library_manager.MovieLibraryManager.add_to_library(self.movie)

	def enter_existing_tag_data(self):
		"""Puts current data into the window"""
		self.title_entry.set_text(self.movie.Title)
		self.genre_cmb.child.set_text(self.movie.Genre)
		self.year_entry.set_text(self.movie.Year)
		self.actors_entry.set_text(self.movie.Actors)
		self.director_entry.set_text(self.movie.Director)
		self.description_entry.get_buffer().set_text(self.movie.Description)

		if self.movie.Rating:
			iter = self.ratings_list.get_iter(constants.ratings.index(self.movie.Rating))
			self.rating_cmb.set_active_iter(iter)

		if self.movie.Artpath:
			self.update_artwork_from_file(self.movie.Artpath)

	def setup_window(self):
		"""Sets up the window with all the entry boxes and comboboxes"""
		tags_box = self.builder.get_object('tags_box')
		title_box = self.builder.get_object('title_val_align')
		genre_box = self.builder.get_object('genre_val_align')
		year_box = self.builder.get_object('year_val_align')
		actors_box = self.builder.get_object('actors_val_align')
		rating_box = self.builder.get_object('rating_val_align')
		director_box = self.builder.get_object('director_val_align')

		self.art_view.set_model(self.art_store)
		self.art_view.set_pixbuf_column(0)
		self.art_view.set_margin(0)
		blank_movie = gtk.gdk.pixbuf_new_from_file('blank_movie.png')
		self.art_store.append((blank_movie,))

		self.title_entry.set_width_chars(22)
		self.year_entry.set_width_chars(22)
		self.actors_entry.set_width_chars(22)
		self.director_entry.set_width_chars(22)
		self.setup_ratings_cmb()
		self.setup_genre_cmb()

		tags_box.hide()

		title_box.add(self.title_entry)
		genre_box.add(self.genre_cmb)
		year_box.add(self.year_entry)
		actors_box.add(self.actors_entry)
		rating_box.add(self.rating_cmb)
		director_box.add(self.director_entry)

		self.enter_existing_tag_data()

		tags_box.show_all()
	
		self.title_entry.grab_focus()
	
	def setup_ratings_cmb(self):
		"""Puts values into the ratings combobox"""
		self.rating_cmb = gtk.ComboBox(self.ratings_list)
		cell = gtk.CellRendererText()
		self.rating_cmb.pack_start(cell, True)
		self.rating_cmb.add_attribute(cell, 'text', 0)

		for rating in constants.ratings:
			self.ratings_list.append((rating,))
	
	def setup_genre_cmb(self):
		"""Puts values into the genres combobox"""
		self.genre_cmb = gtk.ComboBoxEntry(self.genres_list)

		for genre in constants.genres:
			self.genres_list.append((genre,))

	def __init__(self, movie):
		self.movie = movie

		self.builder = gtk.Builder()
		self.builder.add_from_file('movie_tagger.glade') 

		self.tag_window = self.builder.get_object('window')
		self.art_view = self.builder.get_object('art_view')
		self.popup = self.builder.get_object('popup')
		self.paste_item = self.builder.get_object('paste_item')

		self.builder.connect_signals(self)

		self.art_store = gtk.ListStore(gtk.gdk.Pixbuf)
		self.art_path = os.path.join(constants.movie_artwork_dir, 'tmp.png')
		self.artwork_set = False

		self.title_entry = gtk.Entry()
		self.genre_cmb = None
		self.year_entry = gtk.Entry()
		self.actors_entry = gtk.Entry()
		self.rating_cmb = None
		self.director_entry = gtk.Entry()
		self.description_entry = self.builder.get_object('desc_text')

		self.ratings_list = gtk.ListStore(str)
		self.genres_list = gtk.ListStore(str)

		self.clipboard = gtk.Clipboard()
		
		self.setup_window()
		self.tag_window.show()


if __name__ == "__main__":
	app = MovieTagger(movie.Movie(
			path='/home/kris/Videos/Movies/Untagged/Puss in Boots.m4v',
			title='Puss in Boots',
			rating='PG',
			director='Test',
			description='test des',
			genre='Documentary',
			artpath='/home/kris/Downloads/Puss in Boots.jpg'))
	#app.tag_window.show()
	gtk.main()
